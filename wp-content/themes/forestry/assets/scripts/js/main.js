jQuery( document ).ready(function($) {


    const header_scroll = () => {

        $(window).on('scroll', function () {
            let topPosition = 200;
            const scrollTopPosition = $(window).scrollTop();
            if(scrollTopPosition > topPosition){
                $('header').addClass('scrolled-header');
            } else {
                $('header').removeClass('scrolled-header');
            }
        });

    }



    const remove_empty_para_tags = () => {
        $( '.page p:empty, .single p:empty' ).remove();

        $('.twoColumnContent p, .threeColumnMembersContent p, .sectionContactPanel p, .whiteContentPanel p')
            .each(function() {
                var $this = $(this);
                if($this.html().replace(/\s|&nbsp;/g, '').length == 0) $this.remove();
        });

    }


    header_scroll();

    // Remove Empty para tags generated by WP to solve spacing issues for the shortcodes
    remove_empty_para_tags();



});

