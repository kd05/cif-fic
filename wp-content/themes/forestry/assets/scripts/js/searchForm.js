
jQuery( document ).ready(function($) {

    const search_pop = () => {
        $(".search-pop-up-container").toggleClass("pop-up-on");
        $(".search-pop-up-overlay").toggleClass("overlay-pop-up-on");
        $( ".search-field" ).focus();
    }


    // Search Pop Up
    $(".header-search-icon,.search-pop-up-overlay,.close-btn").on("click",search_pop);
    $(".search-submit").on("click",function () {
        $("form.search-form").submit();
    });



});

