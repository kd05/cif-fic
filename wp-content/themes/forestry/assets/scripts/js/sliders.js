jQuery( document ).ready(function($) {


    // Home Page Event Slider
    if($(".eventSliderPanel-slider-container").length){
        // Init Service Slide
        $('.eventSliderPanel-slider-container')
            .slick({
                dots: false,
                arrows: false,
                // infinite: false,
                speed: 900,
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                ]
            });
    }





    // Home Page News/Institute Affairs Slider
    if($(".newsSliderPanel-slider-container").length){
        // Init Service Slide
        $('.newsSliderPanel-slider-container')
            .slick({
                dots: false,
                arrows: false,
                // infinite: false,
                speed: 500,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                ]
            });
    }




    if($(".criteriaForEligibilityPanel-slider-container").length){
        // Init Service Slide
        $('.criteriaForEligibilityPanel-slider-container')
            .slick({
                dots: false,
                arrows: false,
                // infinite: false,
                speed: 900,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                ]
            });
    }




    // sections page News Feed Slider
    if($(".newsFeedSliderPanel-slider-container").length){
        // Init Service Slide
        $('.newsFeedSliderPanel-slider-container')
            .slick({
                dots: false,
                arrows: false,
                speed: 900,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                ]
            });
    }


});

