<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();

if ( have_posts() )
    the_post();

$bg_image_id = get_post_thumbnail_id();
$bannerContent = get_field('banner_content');
?>

<main id="site-content" role="main">

    <?php
        echo do_shortcode("[innerPageBanner image_id='$bg_image_id']"
                .$bannerContent.
            "[/innerPageBanner]");
    ?>

    <?php echo the_content(); ?>

</main><!-- #site-content -->

<?php get_footer(); ?>
