<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>

        </div>
        <!--pageWrapper-->

        <?php
        $frontpage_id = get_option( 'page_on_front' );
        $footer_address = get_field('footer_address', $frontpage_id);
        $footer_contact_info = get_field('footer_contact_info', $frontpage_id);
        $facebook_link = get_field('facebook_link', $frontpage_id);
        $twitter_link = get_field('twitter_link', $frontpage_id);
        $instagram_link = get_field('instagram_link', $frontpage_id);
        $linkedin_link = get_field('linkedin_link', $frontpage_id);
        $youtube_link = get_field('youtube_link', $frontpage_id);
        ?>
        <footer >
            <div class="footerContainer">
                <div class="footer__top">
                    <div class="footer__top-logo footer__top-row">
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer-logo.jpg" alt="">
                        </a>
                    </div>

                    <div class="footer__top-address  footer__top-row">
                        <p class="footer-para"><?php echo nl2br($footer_address); ?></p>
                    </div>

                    <div class="footer__top-contactInfo footer__top-row">
                        <p class="footer-para"><?php echo nl2br($footer_contact_info); ?></p>
                    </div>

                    <div class="footer__top-socialLinks footer__top-row">
                        <ul>
                            <li><a href="<?php echo $facebook_link; ?>"><i class="fab fa-facebook"></i></a></li>
                            <li><a href="<?php echo $twitter_link; ?>"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="<?php echo $instagram_link; ?>"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="<?php echo $linkedin_link; ?>"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="<?php echo $youtube_link; ?>"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="footer__bottom">
                    <div class="footer__bottom-common copyright">
                        <p>Copyright CIF-IFC All Rights Reserved © 2020</p>
                    </div>
                    <div class="footer__bottom-common gp-site">
                        <p>Website by GeekPower Web Design In Toronto</p>
                    </div>
                </div>
            </div>
        </footer>

		<?php wp_footer(); ?>

	</body>
</html>
