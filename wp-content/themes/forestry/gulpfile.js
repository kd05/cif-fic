// npm install --save-dev gulp gulp-sass gulp-sourcemaps gulp-concat gulp-uglify-es
// npm install --save-dev gulp gulp-sass gulp-sourcemaps gulp-concat gulp-uglify-es gulp-postcss autoprefixer cssnano  gulp-uglify gulp-replace

const gulp = require('gulp');
const uglify = require('gulp-uglify-es').default;
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const log = require('fancy-log');


/*
  -- TOP LEVEL FUNCTIONS --
  gulp.task - Define tasks
  gulp.src - Point to files to use
  gulp.dest - Points to folder to output
  gulp.watch - Watch files and folders for changes
*/


// File paths
const files = {
    scssWatchPath: './assets/styles/scss/**/*.scss',
    scssSourcePath: './assets/styles/scss/style.scss',
    scssOutput: './assets/styles/dest/',
    jsPath: './assets/scripts/js/**/*.js',
    jsOutput: './assets/scripts/dest/',
}



const showMessage = (msg) => {
    log(msg);
}


const scssTask = () => {
    return gulp.src(files.scssSourcePath)
        .pipe(sourcemaps.init())
        .pipe(sass.sync({outputStyle: 'compact'}).on('error', sass.logError))
        .pipe(concat('style.min.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(files.scssOutput));
}


// JS Compress and Concate
const jsTask = () => {
    return gulp.src(files.jsPath)
        .pipe(concat('main.min.js'))
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(files.jsOutput))
        .on('end', () => {
            setTimeout(() => { showMessage("******** Gulp task completed *******"); },100);
        });
}



gulp.task('default', gulp.series( gulp.parallel(scssTask, jsTask)));


// ********   Run Command "gulp watch" and save js and css   ********
gulp.task('watch', function(){
    gulp.watch([files.scssWatchPath, files.jsPath],
        gulp.series(gulp.parallel(scssTask, jsTask))
    );
});