<?php

/***************************************************************
 *                          Inc / Functions
 **************************************************************/
require_once(get_template_directory() . '/inc/functions/enqueue-scripts.php');
require_once(get_template_directory() . '/inc/functions/menus.php');
require_once(get_template_directory() . '/inc/functions/hooks.php');
require_once(get_template_directory() . '/inc/functions/gp-functions.php');



/***************************************************************
 *                      Custom Post Types
 **************************************************************/
require_once(get_template_directory() . '/inc/customPostType/section/section.php');





/***************************************************************
 *                          Shortcodes
 **************************************************************/

require_once(get_template_directory() . '/inc/shortcodes/boxesPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/breadcrumb.php');
require_once(get_template_directory() . '/inc/shortcodes/button.php');
require_once(get_template_directory() . '/inc/shortcodes/centerPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/criteriaForEligibilityPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/eventSliderPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/eventsPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/fullWidthImageContent.php');
require_once(get_template_directory() . '/inc/shortcodes/greyBackgroundContent.php');
require_once(get_template_directory() . '/inc/shortcodes/homeBanner.php');
require_once(get_template_directory() . '/inc/shortcodes/imageContentPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/imageTitlePanel.php');
require_once(get_template_directory() . '/inc/shortcodes/innerPageBanner.php');
require_once(get_template_directory() . '/inc/shortcodes/link.php');
require_once(get_template_directory() . '/inc/shortcodes/newsFeedSliderPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/newsSliderPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/sectionContactPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/serviceBoxes.php');
require_once(get_template_directory() . '/inc/shortcodes/socialIcons.php');
require_once(get_template_directory() . '/inc/shortcodes/spacer.php');
require_once(get_template_directory() . '/inc/shortcodes/threeColumnContent.php');
require_once(get_template_directory() . '/inc/shortcodes/threeColumnMembersContent.php');
require_once(get_template_directory() . '/inc/shortcodes/threeImageColumnContent.php');
require_once(get_template_directory() . '/inc/shortcodes/twoColumnContent.php');
require_once(get_template_directory() . '/inc/shortcodes/whiteContentPanel.php');


