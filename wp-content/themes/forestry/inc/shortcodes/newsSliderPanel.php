<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function news_slider_panel_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'ids' => '',
        'title' => 'Institute Affairs',
        'link1_text' => 'View All Posts',
        'link1' => '#',
        'link2_text' => 'View On Twitter',
        'link2' => '#',
        'bg_image' => '192'
    ), $atts);

    ob_start();
    $posts = explode(',', $a['ids']);
    $title = $a['title'];
    $link1_text = $a['link1_text'];
    $link1 = $a['link1'];
    $link2_text = $a['link2_text'];
    $link2 = $a['link2'];
    $bg_image = $a['bg_image'];
    $bg_image_url = wp_get_attachment_url($bg_image, "large");

    ?>
    <div class="newsSliderPanel">
        <div class="newsSliderPanel__wrapper">

            <div class="newsSliderPanel-slider">
                <div class="newsSliderPanel-slider-inner">
                    <h4><?php echo $title ?></h4>
                    <div class="newsSliderPanel-slider-container" >
                        <?php  foreach($posts as $postId) {
                            $eventDate = get_field('slider_date',$postId);
                            $eventTitle = get_the_title($postId);
                            $excerpt = get_the_excerpt($postId);
                            $eventLink = get_the_permalink($postId);
                            ?>
                            <div class="newsSlide">
                                <div class="newsSlide--inner">

                                    <div class="newsSlide-content">
                                        <p class="date"><?php echo $eventDate; ?></p>
                                        <a href="<?php echo $eventLink; ?>">
                                            <h5><?php echo $eventTitle; ?></h5>
                                        </a>
                                        <p><?php echo $excerpt; ?></p>
                                        <a href="" class="learn-more">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php echo do_shortcode("[link text='$link1_text' link='$link1' color='green' ]"); ?>
                </div>
            </div>

            <div class="newsSliderPanel-twitter" style="background-image: url(<?php echo $bg_image_url; ?>)">
                <div class="green-overlay"></div>
                <div class="newsSliderPanel-twitter-inner">
                    <h4>On Twitter</h4>
                    <?php echo do_shortcode($content); ?>
                    <?php echo do_shortcode("[link text='$link2_text' link='$link2' color='white' ]"); ?>
                </div>
            </div>

        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'newsSliderPanel', 'news_slider_panel_shortcode' );
