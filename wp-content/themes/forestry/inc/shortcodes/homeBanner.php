<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

function home_banner_shortcode( $atts, $content = null ) {
    $a= array(
        'image_id' => '8',
        'next_panel_id' => ''
    );
    $a = shortcode_atts($a, $atts);

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img, "large");
    ob_start();

    ?>
    <div class="homeBanner " style="background-image: url(<?php echo $main_img; ?>)">
        <div class="gradient-overlay"></div>
        <div class="homeBanner__content">
            <div class="homeBanner__content-inner">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
        <div class="nxt-panel">
            <i class="far fa-long-arrow-down"></i>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'homeBanner', 'home_banner_shortcode' );


