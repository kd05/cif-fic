<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function sectionContactPanel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_id' => '385'
    ), $atts );
    ob_start();

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img);
    ?>
    <div class="sectionContactPanel" style="background-image: url(<?php echo $main_img; ?>)">
        <div class="sectionContactPanel__wrapper">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'sectionContactPanel', 'sectionContactPanel_shortcode');




function sectionContactPanelContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'title' => 'Quick Links'
    ), $atts );
    ob_start();

    $title = $a['title'];
    ?>
    <div class="sectionContactPanelContent">
        <div class="gradient"></div>
        <div class="sectionContactPanelContent-inner">
            <p class="small-title"> <?php echo $title; ?></p>
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'sectionContactPanelContent', 'sectionContactPanelContent_shortcode' );





