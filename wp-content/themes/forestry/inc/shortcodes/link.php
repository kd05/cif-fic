<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function link_shortcode( $atts, $content = null ) {
    $a= array(
        'text' => 'Read More',
        'page_id' => '',
        'link' => '#',
        'color' => 'white',
        'background' => '',
        'margin_top' => '50'
    );
    $a = shortcode_atts($a, $atts);

    $rand = generateRandomString();

    $text = $a['text'];
    $link = $a['link'];
    $page_id = $a['page_id'];
    if(!empty($page_id)) $link = get_the_permalink($page_id);
    $color = $a['color'];
    $background = "background-".$a['background'];
    ob_start();
    ?>

    <style>
            .gp-responsive-margin-top-<?php echo $rand; ?> {
                margin-top: <?php echo (int) $a['margin_top'] * 1;?>px;
            }
            @media screen and (max-width: 1024px){
                .gp-responsive-margin-top-<?php echo $rand; ?> {
                    margin-top: <?php echo (int) $a['margin_top'] * .7; ?>px;
                }
            }
            @media screen and (max-width: 700px){
                .gp-responsive-margin-top-<?php echo $rand; ?> {
                    margin-top: <?php echo (int) $a['margin_top'] * .6; ?>px;
                }
            }
    </style>
    <div class="linkContainer gp-responsive-margin-top-<?php echo $rand; ?> <?php echo $background; ?>">
        <a class="color-<?php echo $color  ?>" href="<?php echo $link  ?>"><?php echo $text  ?></a>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'link', 'link_shortcode' );


