<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function button_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'color' => 'red',
        'text' => 'READ MORE',
        'link' => '#',
        'page_id' => '',
        'align' => 'left',
        'width' => 'auto'
    ), $atts );
    ob_start();

    $color = $a['color']."Button";
    $text = $a['text'];
    $link = ($a['page_id'] == "") ? $a['link'] : get_permalink($a['page_id']);
    $align = $a['align']."-align";
    $width = $a['width'];
    ?>
    <div class="buttonContainer <?php echo $color." ".$align; ?>">
        <a href="<?php echo $link; ?>" style="min-width: <?php echo $width; ?>"><?php echo $text; ?></a>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'button', 'button_shortcode' );