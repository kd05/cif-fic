<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function whiteContentPanel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(

    ), $atts );
    ob_start();

    ?>
    <div class="whiteContentPanel" >
        <div class="whiteContentPanel__wrapper">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'whiteContentPanel', 'whiteContentPanel_shortcode');




function whiteContentPanelInfo_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'title' => 'Quick Links'
    ), $atts );
    ob_start();

    $title = $a['title'];
    ?>
    <div class="whiteContentPanelInfo">
        <div class="whiteContentPanelInfo-inner">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'whiteContentPanelInfo', 'whiteContentPanelInfo_shortcode' );





