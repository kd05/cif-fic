<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

function fullWidthImageContent_shortcode( $atts, $content = null ) {
    $a= array(
        'image_id' => '8',
        "show_image" => "yes"
    );
    $a = shortcode_atts($a, $atts);

    $show_image = $a['show_image'];
    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img, "large");
    $leftTree = get_template_directory_uri()."/assets/images/tree-left.png";
    ob_start();

    ?>
    <div class="fullWidthImageContent " style="background-image: url(<?php echo $main_img; ?>)">
        <div class="white-bg"></div>
        <div class="white-gradient"></div>
        <?php if(($show_image) == 'yes') { ?>
            <div class="fullWidthImageContent__tree" style="background-image: url(<?php echo $leftTree; ?>)"></div>
        <?php } ?>
        <div class="fullWidthImageContent__content">
            <div class="fullWidthImageContent__content-inner">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'fullWidthImageContent', 'fullWidthImageContent_shortcode' );


