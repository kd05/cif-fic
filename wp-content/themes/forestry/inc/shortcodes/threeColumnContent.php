<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

function threeColumnContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'bg_image_id' => '',
        'padding' => '0px'
    ), $atts );

    $padding = $a['padding'];
    $bg_image_id = $a['bg_image_id'];

    $bg_image = (!empty($bg_image_id)) ?
        wp_get_attachment_url($bg_image_id, "large") : "";

    ob_start();
    ?>
    <div class="threeColumnContent"
         style="padding: <?php echo $padding; ?>;
                background-image: url(<?php echo $bg_image; ?>)">
        <div class="threeColumnContent__wrapper">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'threeColumnContent', 'threeColumnContent_shortcode' );




function singleColumnContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_id' => '124',
    ), $atts );
    ob_start();

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img);
    ?>
    <div class="singleColumnContent">
        <div class="singleColumnContent--inner">
            <div class="singleColumnContent_image">
                <img src="<?php echo $main_img; ?>" alt="">
            </div>
            <div class="singleColumnContent_info">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'singleColumnContent', 'singleColumnContent_shortcode' );
