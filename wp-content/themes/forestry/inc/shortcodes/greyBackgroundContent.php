<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function greyBackgroundContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
            "show_images" => "yes"
    ), $atts );
    ob_start();

    $show_images = $a['show_images'];
    $leftLeaf = get_template_directory_uri()."/assets/images/leaves-left.png";
    $rightLeaf = get_template_directory_uri()."/assets/images/leaves-right.png";
    ?>
    <div class="greyBackgroundContent " >
        <?php if(($show_images) == 'yes') { ?>
            <div class="greyBackgroundContent__left-leaf" style="background-image: url(<?php echo $leftLeaf; ?>)"></div>
            <div class="greyBackgroundContent__right-leaf" style="background-image: url(<?php echo $rightLeaf; ?>)"></div>
        <?php } ?>
        <div class="greyBackgroundContent--wrapper" >
            <div class="greyBackgroundContent--content" >
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'greyBackgroundContent', 'greyBackgroundContent_shortcode' );





function locationIcons_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
    <div class="locationIconsContainer">
        <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'locationIcons', 'locationIcons_shortcode' );





function locationSingleIcon_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        "title" => "",
        "icon" => "",
        "link" => "",
    ), $atts );
    ob_start();
    $title = $a['title'];
    $icon = $a['icon'];
    $link = $a['link'];
    ?>
    <div class="locationIcon">
        <div class="locationIcon__inner">
            <div class="locationIcon__icon">
                <?php if(!empty($icon)) { echo '<i class="'.$icon.'"></i>'; } ?>
            </div>
            <a href="<?php echo $link; ?>"><h5><?php echo $title; ?></h5></a>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'locationSingleIcon', 'locationSingleIcon_shortcode' );










function bannerBottomContentTwo_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
    <div class="bannerBottomContainerTwo " data-aos="fade-right">
        <div class="bannerBottomContainerTwo--inner" >
                <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'bannerBottomContentTwo', 'bannerBottomContentTwo_shortcode' );