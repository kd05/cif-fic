<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function event_slider_panel_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'event_ids' => ''
    ), $atts);

    ob_start();
    $events = explode(',', $a['event_ids']);
    ?>
    <div class="eventSliderPanel">
        <div class="eventSliderPanel__wrapper">
            <div class="eventSliderPanel-content">
                <div class="eventSliderPanel-content-inner">
                    <?php echo do_shortcode($content); ?>
                </div>
            </div>

            <div class="eventSliderPanel-slider">
                <div class="eventSliderPanel-slider-inner">
                    <div class="eventSliderPanel-slider-container" >
                        <?php  foreach($events as $eventId) {
                            $event = get_the_post_thumbnail_url( $eventId, 'full') ;
                            $eventDate = get_field('slider_date',$eventId);
                            $eventTitle = get_the_title($eventId);
                            $eventLink = get_the_permalink($eventId);
                            ?>
                            <div class="eventSlide">
                                <div class="eventSlide--inner">
                                    <div class="eventSlide__image" style=" background-image: url('<?php echo $event; ?>')"></div>
                                    <div class="eventSlide-content">
                                        <p class="date"><?php echo $eventDate; ?></p>
                                        <a href="<?php echo $eventLink; ?>">
                                            <h6><?php echo $eventTitle; ?></h6>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'eventSliderPanel', 'event_slider_panel_shortcode' );
