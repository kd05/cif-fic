<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

function innerPageBanner_shortcode( $atts, $content = null ) {
    $a= array(
        'image_id' => ''
    );
    $a = shortcode_atts($a, $atts);

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img, "large");
    ob_start();
    ?>
    <div class="innerPageBanner" style="background-image: url(<?php echo $main_img; ?>)">
        <div class="innerPageBanner__gradient"></div>
        <div class="innerPageBanner__content container-1270">
            <div class="innerPageBanner__content-wrapper">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'innerPageBanner', 'innerPageBanner_shortcode' );



