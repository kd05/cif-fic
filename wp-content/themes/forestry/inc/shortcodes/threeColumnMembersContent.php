<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

function threeColumnMembersContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
            'title' => 'Council Members'
    ), $atts );
    ob_start();

    $title = $a['title'];
    ?>
    <div class="threeColumnMembersContent">
        <div class="threeColumnMembersContent__title">
            <p class="small-title"><?php echo $title; ?></p>
        </div>
        <div class="threeColumnMembersContent__wrapper">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'threeColumnMembersContent', 'threeColumnMembersContent_shortcode' );




function singleColumnMembersContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_id' => '124',
    ), $atts );
    ob_start();

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img);
    ?>
    <div class="singleColumnMembersContent">
        <div class="singleColumnMembersContent--inner">
            <div class="singleColumnMembersContent_image">
                <img src="<?php echo $main_img; ?>" alt="">
            </div>
            <div class="singleColumnMembersContent_info">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'singleColumnMembersContent', 'singleColumnMembersContent_shortcode' );
