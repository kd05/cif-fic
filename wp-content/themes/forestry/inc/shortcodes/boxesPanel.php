<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function boxes_panel_shortcode( $atts, $content = null ) {
    ob_start();
    ?>
    <div class="boxesPanel">
        <div class="boxesPanel__wrapper">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'boxesPanel', 'boxes_panel_shortcode' );




function box_single_shortcode( $atts, $content = null ) {
    $a= array(
        'image_id' => '162',
    );
    $a = shortcode_atts($a, $atts);

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img, "large");
    ob_start();
    ?>
    <div class="boxSingleContainer" >
        <div class="boxSingleContainer--inner" style="background-image: url(<?php echo $main_img; ?>)">
            <div class="overlay-box"></div>
            <div class="boxSingle__content" >
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'boxSingle', 'box_single_shortcode' );
