<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function imageTitlePanel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_id' => '155',
        'title' => 'Our History'
    ), $atts );
    ob_start();

    $main_img = $a['image_id'];
    $title = $a['title'];
    $main_img = wp_get_attachment_url($main_img);
    ?>
    <div class="imageTitlePanel">

        <div class="imageTitlePanel__image">
            <img src="<?php  echo $main_img; ?>" alt="">
        </div>

        <div class="imageTitlePanel__title">
            <div class="imageTitlePanel__title-inner">
                <h1><?php echo $title; ?></h1>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'imageTitlePanel', 'imageTitlePanel_shortcode' );