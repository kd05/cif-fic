<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function newsFeedSliderPanel_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'post_ids' => ''
    ), $atts);

    ob_start();
    $posts = explode(',', $a['post_ids']);
    ?>
    <div class="newsFeedSliderPanel">
        <div class="newsFeedSliderPanel__wrapper">
            <div class="newsFeedSliderPanel-content">
                <div class="newsFeedSliderPanel-content-inner">
                    <?php echo do_shortcode($content); ?>
                </div>
            </div>

            <div class="newsFeedSliderPanel-slider">
                <div class="newsFeedSliderPanel-slider-inner">
                    <div class="newsFeedSliderPanel-slider-container" >
                        <?php  foreach($posts as $postsId) {
                            $postImg = get_the_post_thumbnail_url( $postsId, 'full') ;
                            $postDate = get_field('slider_date',$postsId);
                            $postTitle = get_the_title($postsId);
                            $postLink = get_the_permalink($postsId);
                            ?>
                            <div class="newsFeedSlide">
                                <div class="newsFeedSlide--inner">
                                    <div class="newsFeedSlide__image" style=" background-image: url('<?php echo $postImg; ?>')"></div>
                                    <div class="newsFeedSlide-content">
                                        <p class="date"><?php echo $postDate; ?></p>
                                        <h6><?php echo $postTitle; ?></h6>
                                        <a href="<?php echo $postLink; ?>">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'newsFeedSliderPanel', 'newsFeedSliderPanel_shortcode' );
