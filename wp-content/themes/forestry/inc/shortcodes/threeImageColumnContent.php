<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

function threeImageColumnContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
    <div class="threeImageColumnContent">
        <div class="threeImageColumnContent__wrapper">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'threeImageColumnContent', 'threeImageColumnContent_shortcode' );




function singleImageColumnContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_id' => '124',
    ), $atts );
    ob_start();

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img);
    ?>
    <div class="singleImageColumnContent">
        <div class="singleImageColumnContent--inner">
            <div class="singleImageColumnContent_image">
                <img src="<?php echo $main_img; ?>" alt="">
            </div>
            <div class="singleImageColumnContent_info">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'singleImageColumnContent', 'singleImageColumnContent_shortcode' );
