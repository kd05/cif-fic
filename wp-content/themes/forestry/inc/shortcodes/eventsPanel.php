<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function eventsPanel_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'bg_image' => '360',
        'post_ids' => '',
        'title' => 'Events',
        'sub_title' => 'Get Involved'
    ), $atts);

    ob_start();
    $posts = explode(',', $a['post_ids']);
    $title = $a['title'];
    $sub_title = $a['sub_title'];
    $bg_image = $a['bg_image'];
    $bg_image_url = wp_get_attachment_url($bg_image, "large");
    ?>
    <div class="eventsPanel"  style="background-image: url(<?php echo $bg_image_url; ?>)">
        <div class="eventsPanel__wrapper">

            <div class="eventsPanel__top-content">
                <p class="small-title"><?php echo $title; ?></p>
                <h2><?php echo $sub_title; ?></h2>
                <?php // echo do_shortcode($content); ?>
            </div>

            <div class="eventsPanel-listing" >
                <?php  foreach($posts as $postsId) {
                    $postTitle = get_the_title($postsId);
                    $postExcerpt = get_the_excerpt($postsId);
                    $postLink = get_the_permalink($postsId);
                    ?>
                    <div class="eventsPanel__single">
                        <div class="eventsPanel__single-content">
                            <h4><?php echo $postTitle; ?></h4>
                            <p><?php echo $postExcerpt; ?></p>
                            <a class="small-title view-event" href="<?php echo $postLink; ?>">VIEW EVENT</a>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="eventsPanel__bottom-content">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'eventsPanel', 'eventsPanel_shortcode' );
