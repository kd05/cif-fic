<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function service_boxes_shortcode( $atts, $content = null ) {
    $a= array(
        'bg_image' => '8',
        'id' => ''
    );
    $a = shortcode_atts($a, $atts);

    $main_img = $a['bg_image'];
    $main_img = wp_get_attachment_url($main_img, "large");
    ob_start();

    ?>
    <div class="serviceBoxes">
        <div class="image-overlay" style="background-image: url(<?php echo $main_img; ?>)"></div>
        <div class="serviceBoxes__wrapper">
            <div class="serviceBoxes__wrapper-inner">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'serviceBoxes', 'service_boxes_shortcode' );




function service_single_shortcode( $atts, $content = null ) {
    $a= array(
        'image_id' => '147',
        'title' => 'No Title',
        'sub_title' => 'No sub title',
        'page_id' => '',
        'link' => '#',
    );
    $a = shortcode_atts($a, $atts);

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img, "large");
    $title = $a['title'];
    $sub_title = $a['sub_title'];
    $link = $a['link'];
    $page_id = $a['page_id'];
    if(!empty($page_id)) $link = get_the_permalink($page_id);

    ob_start();

    ?>
    <div class="serviceSingle">
        <div class="serviceSingle__image" style="background-image: url(<?php echo $main_img; ?>)">
            <a href="<?php echo $link; ?>"></a>
        </div>
        <div class="serviceSingle__content">
            <a href="<?php echo $link; ?>"><h3><?php echo $title; ?></h3></a>
            <p><?php echo $sub_title; ?></p>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'serviceSingle', 'service_single_shortcode' );
