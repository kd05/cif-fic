<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function twoColumnContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
            'show_images' => 'yes'
    ), $atts );
    ob_start();
    $show_images = $a['show_images'];
    $leftLeaves = get_template_directory_uri()."/assets/images/sl-left.png";
    $rightLeaves = get_template_directory_uri()."/assets/images/sl-right.png";
    $bottomLeaves = get_template_directory_uri()."/assets/images/sl-bottom.png";
    ?>
    <div class="twoColumnContent">
        <?php if(($show_images) == 'yes') { ?>
            <div class="twoColumnContent__left-image" style="background-image: url(<?php echo $leftLeaves; ?>)"></div>
            <div class="twoColumnContent__right-image" style="background-image: url(<?php echo $rightLeaves; ?>)"></div>
            <div class="twoColumnContent__bottom-image" style="background-image: url(<?php echo $bottomLeaves; ?>)"></div>
        <?php } ?>
        <div class="twoColumnContent__wrapper">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'twoColumnContent', 'twoColumnContent_shortcode' );




function singleTwoColumnContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
            "animation" => "right"
    ), $atts );
    ob_start();
    ?>
    <div class="singleColumnContent"  <?php echo ' data-aos="fade-'.$a['animation'].'" ';  ?>>
        <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'singleTwoColumnContent', 'singleTwoColumnContent_shortcode' );
