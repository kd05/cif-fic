<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function socialIcons_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'facebook' => '',
        'twitter' => '',
        'instagram' => '',
        'linkedin' => '',
        'youtube' => ''
    ), $atts );
    ob_start();

    $facebook = $a['facebook'];
    $twitter = $a['twitter'];
    $instagram = $a['instagram'];
    $linkedin = $a['linkedin'];
    $youtube = $a['youtube'];
    ?>
    <div class="socialIcons_shortcode">
        <ul>
            <?php if(!empty($facebook)) { ?><li><a href="<?php echo $facebook; ?>"><i class="fab fa-facebook"></i></a></li> <?php } ?>
            <?php if(!empty($twitter)) { ?><li><a href="<?php echo $twitter; ?>"><i class="fab fa-twitter"></i></a></li><?php } ?>
            <?php if(!empty($instagram)) { ?><li><a href="<?php echo $instagram; ?>"><i class="fab fa-instagram"></i></a></li><?php } ?>
            <?php if(!empty($linkedin)) { ?><li><a href="<?php echo $linkedin; ?>"><i class="fab fa-linkedin-in"></i></a></li><?php } ?>
            <?php if(!empty($youtube)) { ?><li><a href="<?php echo $youtube; ?>"><i class="fab fa-youtube"></i></a></li><?php } ?>
        </ul>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'socialIcons', 'socialIcons_shortcode' );