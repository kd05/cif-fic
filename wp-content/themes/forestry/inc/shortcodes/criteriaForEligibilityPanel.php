<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function criteriaForEligibilityPanel_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'event_ids' => ''
    ), $atts);
    ob_start();

    $left_content = get_field('left_content');
    $right_content = get_field('right_content');
    $right_image = (get_field('right_image'))['url'];

//    echo "<pre>";print_r($right_image);echo "</pre>";
    ?>
    <div class="criteriaForEligibilityPanel">
        <div class="criteriaForEligibilityPanel__wrapper">
            <div class="criteriaForEligibilityPanel-content">
                <div class="criteriaForEligibilityPanel-content-inner">
                    <?php echo do_shortcode($left_content); ?>
                </div>
            </div>

            <div class="criteriaForEligibilityPanel-slider"  style="background-image: url(<?php echo $right_image; ?>)">
                <div class="criteriaForEligibilityPanel-slider-inner">
                    <div class="criteriaForEligibilityPanel-topContent">
                        <p><?php echo do_shortcode($right_content); ?></p>
                    </div>
                    <div class="criteriaForEligibilityPanel-slider-container" >
                        <?php  for($i = 1; $i <=5 ; $i++) {
                            $step_content = get_field('step_'.$i);
                            ?>
                            <div class="criteriaForEligibilitySlide">
                                <div class="criteriaForEligibilitySlide--inner">
                                    <div class="criteriaForEligibilitySlide-content">
                                        <h1>0<?php echo $i; ?>.</h1>
                                       <p><?php echo do_shortcode($step_content); ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'criteriaForEligibilityPanel', 'criteriaForEligibilityPanel_shortcode' );
