<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function image_content_panel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_id' => '155',
        'title' => 'Our History',
        'background_color' => '#f1f1f1'
    ), $atts );
    ob_start();

    $main_img = $a['image_id'];
    $title = $a['title'];
    $main_img = wp_get_attachment_url($main_img);
    $background_color = $a['background_color'];
    ?>
    <div class="imageContentPanel">
        <div class="imageContentPanel__image imageContentPanel__<?php echo $background_color; ?>" style="background-image: url('<?php echo $main_img; ?>')"></div>

        <div class="imageContentPanel__info" style="background-color: <?php echo $background_color; ?>">
            <div class="imageContentPanel__info-inner">
                <p class="small-title"><?php echo $title; ?></p>
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'imageContentPanel', 'image_content_panel_shortcode' );