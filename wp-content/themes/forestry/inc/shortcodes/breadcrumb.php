<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function breadcrumb_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'position' => 'relative',
        'page_ids' => ''
    ), $atts );
    ob_start();

    $page_ids = !empty($a['page_ids']) ? explode(',', $a['page_ids']) : '';
    $currentPageId = get_the_ID();
    $parentPageId = wp_get_post_parent_id($currentPageId);
    ?>
    <div class="breadcrumbContainer <?php echo "position-".$a['position'] ?>">
        <div class="breadcrumbContainer-inner">
            <a href="<?php echo site_url(); ?>">CIF-IFC</a>
            <span><i class="fal fa-chevron-right"></i></span>
            <?php if(is_array($page_ids) && count($page_ids) > 0) {
                foreach ($page_ids as $page_id) {
                    get_breadcrumb_link($page_id);
                 }
            } else {
                get_breadcrumb_link($parentPageId);
            }
            ?>
            <a href="javascript:void(0)" class="current-page"><?php echo get_the_title(); ?></a>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'breadcrumb', 'breadcrumb_shortcode' );



function get_breadcrumb_link($pageId){
    $title = get_the_title($pageId);
    $link = get_the_permalink($pageId);
    ?>
    <a href="<?php echo $link; ?>"><?php echo $title; ?></a>
    <span><i class="fal fa-chevron-right"></i></span>
    <?php
}

