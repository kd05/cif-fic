<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



// Register menus
register_nav_menus(
    array(
        'main-nav'      => __('The Main Menu', 'forestry'),
        'top-nav'      => __('The Top Menu', 'forestry'),
    )
);

