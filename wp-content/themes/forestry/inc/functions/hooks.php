<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



/************************************************************
* Hide Admin Bar Front Side
*************************************************************/
add_filter('show_admin_bar', '__return_false');




/************************************************************
 * Disable Auto Update
 *************************************************************/
add_filter( 'auto_update_plugin', '__return_false' );



/************************************************************
 * Post Thumbnails
 *************************************************************/
add_theme_support( 'post-thumbnails' );



/************************************************************
 * Add Custom Image size
 *************************************************************/
//add_image_size( 'large-1500', 1500, 1500, false );
//add_image_size( 'medium-700', 700, 410, false );

