<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function global_scripts()
{
    //    Google fonts
    wp_enqueue_style('montserrat-font', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap');

    //    Font Awesome
    wp_enqueue_style('font-awesome5-pro', get_template_directory_uri() . '/assets/styles/dest/font-awesome/css/all.min.css', array(), false, 'all');


    /****************************************************************
     *                       Slick JS/CSS
     *****************************************************************/
    //    Slick JS
    wp_register_script('slick-js', get_template_directory_uri() . '/assets/scripts/dest/slick/slick.min.js', array('jquery'), false, true );
    wp_enqueue_script( 'slick-js');

    //    Slick Css
    wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/styles/dest/slick/slick.css' );
    wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/assets/styles/dest/slick/slick-theme.css' );


    /****************************************************************
     *                    Custom Styles / Js
     *****************************************************************/
    //    Main Css
    wp_enqueue_style('main-style', get_template_directory_uri() . '/assets/styles/dest/style.min.css', array(), false, 'all');

    //    Main JS
    wp_register_script('main-js', get_template_directory_uri() . '/assets/scripts/dest/main.min.js', array('jquery'), false, true);
    wp_enqueue_script( 'main-js');



}
add_action('wp_enqueue_scripts', 'global_scripts');