<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<link rel="profile" href="https://gmpg.org/xfn/11">
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>


		<header >
            <div class="headerMain">
                <div class="headerMain__left">
                    <a href="<?php echo site_url(); ?>">
                        <img class="logo-white" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="">
                        <img class="logo-black" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-black.png" alt="">
                    </a>
                </div>

                <div class="headerMain__right">

                    <div class="headerMain__right-top">

                        <div class="headerMain__right-top__wrapper">
                            <div class="topMenu">
                                <?php
                                wp_nav_menu(array(
                                    'container'      => false,
                                    'theme_location' => 'top-nav'
                                ));
                                ?>
                            </div>

                            <div class="topSearch header-search-icon">
                                <i class="far fa-search"></i>
                                <span>Search</span>
                            </div>
                        </div>

                    </div>

                    <div class="headerMain__right-bottom">

                        <?php
                        wp_nav_menu(array(
                            'container'      => false,
                            'theme_location' => 'main-nav'
                        ));
                        ?>

                        <div class="headerMain__mobile" id="moby-button">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>

                    </div>

                </div>

            </div>
		</header><!-- #site-header -->

        <!--    Search Form Pop Up-->
        <?php  get_template_part( 'template-parts/search-form/content', 'search-form-gp' );  ?>


        <div class="pageWrapper">