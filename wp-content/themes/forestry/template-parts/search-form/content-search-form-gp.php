<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="search-pop-up-overlay"></div>
<div class="search-pop-up-container">
    <div class="search-wrap">
        <?php echo get_search_form(); ?>
        <div class="search-btn button">
            <a class="search-submit" href="javascript:void(0);"><i class="fa fa-search"></i></a>
        </div>
    </div>
    <div class="close-btn">
        <i class="far fa-times"></i>
    </div>
</div>